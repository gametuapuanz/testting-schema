const dbHandler = require('./db-handler')
const Company = require('../model/companyModel')
const User = require('../model/usersModel')

// Connect to a new in-memory database before running any tests.
beforeAll(async () => {
  await dbHandler.connect()
})

// Clear all test data after every test.
afterEach(async () => {
  await dbHandler.clearDatabase()
})

// Complete company example.
const loginComplete = {
  username: 'admin',
  password: 'password'
}
const loginErrAll = {
  username: '',
  password: ''
}
const loginErrAll2 = {
  username: 'ad',
  password: 'pa'
}
const loginErrPass = {
  username: 'admin',
  password: 'pa'
}
const loginErrUsername = {
  username: 'ad',
  password: 'password'
}
// Complete company example.
const companyComplete1 = {
  companyName: 'CDG',
  tel: '0963652516',
  typeBusiness: 'IT Solution',
  address: '202 ถ.นางลิ้นจี แขวงช่องนนทรี เขต ยานนาวา จ. กรุงเทพ 1012'
}
const companyErrAll = {
  companyName: '',
  tel: '',
  typeBusiness: '',
  address: ''
}
const companyErrName = {
  companyName: '',
  tel: '0963652516',
  typeBusiness: 'IT Solution',
  address: '202 ถ.นางลิ้นจี แขวงช่องนนทรี เขต ยานนาวา จ. กรุงเทพ 1012'
}
const companyErrTel = {
  companyName: 'CDG',
  tel: '',
  typeBusiness: 'IT Solution',
  address: '202 ถ.นางลิ้นจี แขวงช่องนนทรี เขต ยานนาวา จ. กรุงเทพ 1012'
}
const companyErrTel2 = {
  companyName: 'CDG',
  tel: '0455',
  typeBusiness: 'IT Solution',
  address: '202 ถ.นางลิ้นจี แขวงช่องนนทรี เขต ยานนาวา จ. กรุงเทพ 1012'
}
const companyErrTel3 = {
  companyName: 'CDG',
  tel: '09636525161',
  typeBusiness: 'IT Solution',
  address: '202 ถ.นางลิ้นจี แขวงช่องนนทรี เขต ยานนาวา จ. กรุงเทพ 1012'
}
const companyErrType = {
  companyName: 'CDG',
  tel: '0963652516',
  typeBusiness: '',
  address: '202 ถ.นางลิ้นจี แขวงช่องนนทรี เขต ยานนาวา จ. กรุงเทพ 1012'
}
const companyErrAddress = {
  companyName: 'CDG',
  tel: '0963652516',
  typeBusiness: 'IT Solution',
  address: ''
}
// Company test suite.
describe('Company ', () => {
  it('สามารถเพิ่ม company ได้', async () => {
    let error = null
    try {
      const company = new Company(companyComplete1)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
})

// Company test error suite.
describe('Company ', () => {
  it('ไม่สามารถเพิ่ม company ได้เพราะ ไม่มีข้อมูล', async () => {
    let error = null
    try {
      const company = new Company(companyErrAll)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
  it('ไม่สามารถเพิ่ม company ได้เพราะ ชื่อบริษัท เป็นช่องว่าง', async () => {
    let error = null
    try {
      const company = new Company(companyErrName)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })

  it('ไม่สามารถเพิ่ม company ได้เพราะ เบอร์โทร เป็นช่องว่าง', async () => {
    let error = null
    try {
      const company = new Company(companyErrTel)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
  it('ไม่สามารถเพิ่ม company ได้เพราะ เบอร์โทร มีไม่ถึง 10 ตัวอักษร', async () => {
    let error = null
    try {
      const company = new Company(companyErrTel2)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })

  it('ไม่สามารถเพิ่ม company ได้เพราะ เบอร์โทร มีมากกว่า 10 ตัวอักษร', async () => {
    let error = null
    try {
      const company = new Company(companyErrTel3)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
  it('ไม่สามารถเพิ่ม company ได้เพราะ ชนิดของธุรกิจ เป็นช่องว่าง', async () => {
    let error = null
    try {
      const company = new Company(companyErrType)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
  it('ไม่สามารถเพิ่ม company ได้เพราะ ที่อยู่ เป็นช่องว่าง', async () => {
    let error = null
    try {
      const company = new Company(companyErrAddress)
      await company.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
})

// Login test suite.
describe('User ', () => {
  it('สามารถเข้าสู่ระบบได้ ได้', async () => {
    let error = null
    try {
      const user = new User(loginComplete)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
})

// Login test err suite.
describe('User ', () => {
  it('สามารถเข้าสู่ระบบได้ ได้', async () => {
    let error = null
    try {
      const user = new User(loginComplete)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเข้าสู่ระบบได้ ได้เพราะ ไม่มีข้อมูล', async () => {
    let error = null
    try {
      const user = new User(loginErrAll)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
  it('ไม่สามารถเข้าสู่ระบบได้ ได้เพราะ password ผิด', async () => {
    let error = null
    try {
      const user = new User(loginErrPass)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })

  it('ไม่สามารถเข้าสู่ระบบได้ ได้เพราะ username และ password ผิด', async () => {
    let error = null
    try {
      const user = new User(loginErrAll2)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
  it('ไม่สามารถเข้าสู่ระบบได้ ได้เพราะ username ผิด', async () => {
    let error = null
    try {
      const user = new User(loginErrUsername)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error)
  })
})
