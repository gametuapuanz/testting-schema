const mongoose = require('mongoose')

mongoose.connect(
  'mongodb+srv://rootza:1234@mydb-sfzjj.mongodb.net/cooperative?retryWrites=true&w=majority',
  {
    // mongoose.connect('mongodb://root:1234@localhost', {
    useNewUrlParser: true,
    seUnifiedTopology: true
  }
)

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error'))
db.once('open', function (err) {
  if (err) throw err
  console.log('Connect database success')
})

module.exports = db
