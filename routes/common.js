var express = require('express')
var router = express.Router()
var commonController = require('../controller/common')

/* GET users listing. */
router.get('/', commonController.getAllCommon)
router.get('/aboutCooperative', commonController.getAboutCooperative)
router.get('/conditionCooperative', commonController.getConditionCooperative)
router.get('/stepCooperative', commonController.getStepCooperative)

router.put('/aboutCooperative', commonController.updateAboutCooperative)
router.put('/conditionCooperative', commonController.updateConditionCooperative)
router.put('/stepCooperative', commonController.updateStepCooperative)

module.exports = router
