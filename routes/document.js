var express = require('express')
var multer = require('multer')
var router = express.Router()
var documentController = require('../controller/document')

var path = require('path')
var upload = multer({
  dest: './public/uploads',
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, '../public/uploads'))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
  })
})

/* GET users listing. */
router.get('/', documentController.getDocuments)
router.get('/:id', documentController.getDocument)
router.post('/', upload.single('pathImage'), documentController.addDocument)
router.put('/', documentController.getDocument)
router.delete('/:id', documentController.deleteDocument)

module.exports = router
