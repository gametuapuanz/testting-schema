var express = require('express')
var router = express.Router()
var companyController = require('../controller/company')

/* GET users listing. */
router.get('/', companyController.getCompanys)

router.get('/:id', companyController.getCompany)
router.post('/', companyController.addCompany)

router.put('/', companyController.updateCompany)
router.delete('/:id', companyController.deleteCompany)
module.exports = router
