var express = require('express')
var router = express.Router()
var meetingController = require('../controller/meeting')

/* GET users listing. */
router.get('/', meetingController.getMeetings)

router.get('/:id', meetingController.getMeeting)
router.post('/', meetingController.addMeeting)

router.put('/', meetingController.updateMeeting)
router.delete('/:id', meetingController.deleteMeeting)
module.exports = router
