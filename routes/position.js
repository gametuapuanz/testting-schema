var express = require('express')
var router = express.Router()
var positionController = require('../controller/positions')

/* GET skills listing. */
router.get('/', positionController.getPositions)
router.post('/', positionController.addPosition)
router.put('/', positionController.updatePosition)
router.delete('/:id', positionController.deletePosition)

module.exports = router
