var express = require('express')
var router = express.Router()
var meetingJoinController = require('../controller/meetingJoin')

/* GET users listing. */
router.get('/', meetingJoinController.getMeetingJoins)
router.get('/:id', meetingJoinController.getMeetingJoin)
router.get('/meeting/:id', meetingJoinController.getMeetingJoinByMeeting)
router.get('/user/:id', meetingJoinController.getMeetingJoinByUser)
router.post('/', meetingJoinController.addMeetingJoin)
router.put('/', meetingJoinController.updateMeetingJoin)
router.delete('/:id', meetingJoinController.deleteMeetingJoin)

module.exports = router
