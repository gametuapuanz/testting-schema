var express = require('express')
var router = express.Router()
var internshipController = require('../controller/internship')

router.get('/', internshipController.getInternships)

// router.get('/:id', internshipController.getInternship)
router.get('/:id', internshipController.getInternshipByStudent)

router.post('/', internshipController.addInternship)

router.put('/', internshipController.updateInternship)
router.delete('/:id', internshipController.deleteInternship)



module.exports = router
