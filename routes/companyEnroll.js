var express = require('express')
var router = express.Router()
var companyEnrollController = require('../controller/companyEnroll')

/* GET users listing. */
router.get('/', companyEnrollController.getCompanyEnrolls)

router.get('/:id', companyEnrollController.getCompanyEnroll)
router.post('/', companyEnrollController.addCompanyEnroll)

router.put('/', companyEnrollController.updateCompanyEnroll)
router.delete('/:id', companyEnrollController.deleteCompanyEnroll)
module.exports = router
