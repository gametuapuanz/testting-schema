var express = require('express')
var router = express.Router()
var userController = require('../controller/users')
var studentController = require('../controller/students')

/* GET users listing. */
router.post('/login', userController.getLogin)
router.get('/:id', studentController.getStudent)

module.exports = router
