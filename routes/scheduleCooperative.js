var express = require('express')
var router = express.Router()
var scheduleCooperativeController = require('../controller/scheduleCooperative')

/* GET users listing. */
router.get('/', scheduleCooperativeController.getScheduleCooperatives)

router.get('/:id', scheduleCooperativeController.getScheduleCooperative)
router.post('/', scheduleCooperativeController.addScheduleCooperative)

router.put('/', scheduleCooperativeController.updateScheduleCooperative)
router.delete('/:id', scheduleCooperativeController.deleteScheduleCooperative)
module.exports = router
