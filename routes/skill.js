var express = require('express')
var router = express.Router()
var skillController = require('../controller/skills')

/* GET skills listing. */
router.get('/', skillController.getSkills)
router.post('/', skillController.addSkill)
router.put('/', skillController.updateSkill)
router.delete('/:id', skillController.deleteSkill)

module.exports = router
