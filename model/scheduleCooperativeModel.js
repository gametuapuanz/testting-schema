const mongoose = require('mongoose')
const Schema = mongoose.Schema

const scheduleCooperativeSchema = new Schema({
  title: {
    type: 'String'
  },
  student: {
    type: 'Boolean',
    default: false
  },
  instructor: {
    type: 'Boolean',
    default: false
  },
  dean: {
    type: 'Boolean',
    default: false
  },
  company: {
    type: 'Boolean',
    default: false
  },
  date1: {
    type: 'Date'
  },
  date2: {
    type: 'Date'
  }
}, { timestamps: true })

module.exports = mongoose.model(
  'schedulecooperatives',
  scheduleCooperativeSchema
)
