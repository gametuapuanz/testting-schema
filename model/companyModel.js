const mongoose = require('mongoose')
const Schema = mongoose.Schema

const companySchema = new Schema(
  {
    companyName: {
      type: 'String',
      minlength: 1,
      required: true
    },
    address: {
      type: 'String',
      minlength: 1,
      required: true
    },
    tel: {
      type: 'String',
      minlength: 10,
      required: true
    },
    typeBusiness: {
      type: 'String',
      minlength: 1,
      required: true
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('companys', companySchema)
