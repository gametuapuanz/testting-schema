const mongoose = require('mongoose')
const Schema = mongoose.Schema

const internshipSchema = new Schema({
  student: {
    type: 'ObjectId',
    ref: 'users'
  },
  companyEnroll: {
    type: 'ObjectId',
    ref: 'companyenrolls'
  },
  position: {
    type: 'String'
  },
  detail: {
    type: 'String',
    default: ''
  },
  dateAttend: {
    type: 'Date',
    default: new Date()
  },
  statusAttend: {
    type: 'Number',
    default: 1
  }
}, { timestamps: true })

module.exports = mongoose.model('internships', internshipSchema)
