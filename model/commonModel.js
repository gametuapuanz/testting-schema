const mongoose = require('mongoose')
const Schema = mongoose.Schema

const commonSchema = new Schema({
  aboutCooperative: String,
  scheduleCooperative: String,
  scheduleMeeting: String,
  conditionCooperative: String,
  stepCooperative: String
}, { timestamps: true })

module.exports = mongoose.model('commons', commonSchema)
