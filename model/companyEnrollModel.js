const mongoose = require('mongoose')
const Schema = mongoose.Schema

const companyEnrollSchema = new Schema(
  {
    company: {
      type: 'ObjectId',
      ref: 'companys'
    },
    title: {
      type: 'String',
      default: ''
    },
    requirePosition: {
      type: ['String']
    },
    getAmount: {
      type: 'Number',
      default: 0
    },
    getAllAmount: {
      type: 'Number'
    },
    skill: {
      type: ['String']
    },
    dayWork: {
      type: 'Number'
    },
    date1: {
      type: 'Date',
      default: 0
    },
    date2: {
      type: 'Date',
      default: 0
    },
    service: {
      type: 'String'
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('companyenrolls', companyEnrollSchema)
