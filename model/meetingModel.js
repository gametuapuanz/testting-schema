const mongoose = require('mongoose')
const Schema = mongoose.Schema

const meetingSchema = new Schema({
  company: {
    type: 'ObjectId',
    ref: 'companys'
  },
  topics: {
    type: 'String'
  },
  room: {
    type: 'String'
  },
  meetingType: {
    type: 'String'
  },
  time1: {
    type: 'String'
  },
  time2: {
    type: 'String'
  },
  registDate1: {
    type: 'Date'
  },
  registDate2: {
    type: 'Date'
  },
  meetingDate1: {
    type: 'Date'
  },
  meetingDate2: {
    type: 'Date'
  },
  amountStudent: {
    type: 'Number'
  },
  getAmountStudent: {
    type: 'Number',
    default: 0
  },
  hours: {
    type: 'Number'
  }
}, { timestamps: true })

module.exports = mongoose.model('meetings', meetingSchema)
