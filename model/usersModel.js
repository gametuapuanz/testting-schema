const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema(
  {
    firstname: {
      type: 'String'
    },
    lastname: {
      type: 'String'
    },
    tel: {
      type: 'String'
    },
    address: {
      type: 'String'
    },
    studentId: {
      type: 'String'
    },
    gpa: {
      type: 'Number'
    },
    major: {
      type: 'String'
    },
    hoursMeetingTechnic: {
      type: 'Number'
    },
    hoursMeetingPrepare: {
      type: 'Number'
    },
    statusCooperative: {
      type: 'Number'
    },
    username: {
      type: 'String',
      required: true
    },
    password: {
      type: 'String',
      required: true
    },
    type: {
      type: 'String'
    },
    grade: {
      S88510059: {
        type: 'String'
      },
      S88510259: {
        type: 'String'
      },
      S88510459: {
        type: 'String'
      },
      S88620159: {
        type: 'String'
      },
      S88620259: {
        type: 'String'
      },
      S88620359: {
        type: 'String'
      },
      S88620459: {
        type: 'String'
      },
      S88621159: {
        type: 'String'
      },
      S88631159: {
        type: 'String'
      },
      S88634159: {
        type: 'String'
      }
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('users', userSchema)
