const mongoose = require('mongoose')
const Schema = mongoose.Schema

const documentSchema = new Schema({
  documentName: {
    type: 'String'
  },
  pathImage: {
    type: 'String'
  },
  dateUpload: {
    type: 'Date'
  }
}, { timestamps: true })

module.exports = mongoose.model('documents', documentSchema)
