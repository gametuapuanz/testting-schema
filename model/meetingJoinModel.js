const mongoose = require('mongoose')
const Schema = mongoose.Schema

const meetingJoinSchema = new Schema({
  student: {
    type: 'ObjectId',
    ref: 'users'
  },
  meeting: {
    type: 'ObjectId',
    ref: 'meetings'
  },
  statusJoin: {
    type: 'Boolean'
  },
  dateJoin: {
    type: 'Date'
  },
  timeJoin: {
    type: 'String'
  },
  statusCheckName: {
    type: 'Boolean'
  }
}, { timestamps: true })

module.exports = mongoose.model('meetingjoins', meetingJoinSchema)
