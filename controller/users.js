const User = require('./../model/usersModel')

module.exports = {
  async getLogin (req, res, next) {
    const username = req.body.username
    const password = req.body.password
    await User.find({ username: username, password: password }).then(data => {
      res.json(data)
    })
  }
}
