const CompanyEnroll = require('./../model/companyEnrollModel')

module.exports = {
  async getCompanyEnrolls (req, res, next) {
    await CompanyEnroll.find(
      {},
      {},
      {
        sort: {
          date1: -1
        }
      }
    )
      .populate('company')
      .then(data => {
        res.json(data)
      })
  },
  async getCompanyEnroll (req, res, next) {
    const companyEnrollId = req.params.id
    await CompanyEnroll.findById(companyEnrollId).then(data => {
      res.json(data)
    })
  },
  async addCompanyEnroll (req, res, next) {
    const companyEnroll = new CompanyEnroll(req.body)
    await companyEnroll.save().then(data => {
      res.json(data)
    })
  },
  async updateCompanyEnroll (req, res, next) {
    const payload = req.body
    await CompanyEnroll.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteCompanyEnroll (req, res, next) {
    const companyEnrollId = req.params.id
    await CompanyEnroll.findByIdAndDelete(companyEnrollId).then(data => {
      res.json(data)
    })
  }
}
