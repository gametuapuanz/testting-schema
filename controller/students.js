const Student = require('./../model/usersModel')

module.exports = {
  async getStudents (req, res, next) {
    await Student.find({ type: 'student' }).then(data => {
      res.json(data)
    })
  },
  async getStudent (req, res, next) {
    const studentId = req.params.id
    if (studentId.length === 8) {
      await Student.findOne({ studentId: studentId }).then(data => {
        res.json(data)
      })
    } else {
      await Student.findById(studentId).then(data => {
        if (data.statusCooperative === 4) {
        }
        res.json(data)
      })
    }
  },
  async addStudent (req, res, next) {
    const student = new Student(req.body)
    await student.save().then(data => {
      res.json(data)
    })
  },
  async updateStudent (req, res, next) {
    const payload = req.body
    await Student.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteStudent (req, res, next) {
    const studentId = req.params.id
    await Student.findByIdAndDelete(studentId).then(data => {
      res.json(data)
    })
  }
}
