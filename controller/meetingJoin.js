const MeetingJoin = require('./../model/meetingJoinModel')
const Meeting = require('./../model/meetingModel')
const User = require('./../model/usersModel')

module.exports = {
  async getMeetingJoins (req, res, next) {
    await MeetingJoin.find()
      .populate('student')
      .populate('meeting')
      .then(data => {
        res.json(data)
      })
  },
  async getMeetingJoin (req, res, next) {
    const meetingJoinId = req.params.id
    await MeetingJoin.findById(meetingJoinId).then(data => {
      res.json(data)
    })
  },
  async getMeetingJoinByMeeting (req, res, next) {
    const meetingId = req.params.id
    await MeetingJoin.find({ meeting: meetingId })
      .populate('student')
      .populate('meeting')
      .then(data => {
        res.json(data)
      })
  },
  async getMeetingJoinByUser (req, res, next) {
    const userId = req.params.id
    await MeetingJoin.find({ student: userId })
      .populate({ path: 'meeting', populate: 'company' })
      .then(data => {
        res.json(data)
      })
  },
  async addMeetingJoin (req, res, next) {
    console.log('Add meetingJoin')
    console.log(req.body)
    const meetJoin = new MeetingJoin(req.body)
    await meetJoin.save().then(async data => {
      await Meeting.findOneAndUpdate(
        { _id: data.meeting },
        {
          $inc: { getAmountStudent: 1 }
        }
      )
      res.json(data)
    })
  },
  async updateMeetingJoin (req, res, next) {
    const payload = req.body
    await MeetingJoin.findByIdAndUpdate(payload._id, payload).then(
      async data => {
        if (data.statusCheckName !== payload.statusCheckName) {
          if (payload.statusCheckName === true) {
            if (payload.meeting.meetingType === 'เตรียมความพร้อม') {
              await User.findOneAndUpdate(
                { _id: data.student },
                {
                  $inc: { hoursMeetingPrepare: payload.meeting.hours }
                }
              )
            } else if (payload.meeting.meetingType === 'วิชาการ') {
              await User.findOneAndUpdate(
                { _id: data.student },
                {
                  $inc: { hoursMeetingTechnic: payload.meeting.hours }
                }
              )
            }
          } else {
            if (payload.meeting.meetingType === 'เตรียมความพร้อม') {
              await User.findOneAndUpdate(
                { _id: data.student },
                {
                  $inc: { hoursMeetingPrepare: -payload.meeting.hours }
                }
              )
            } else if (payload.meeting.meetingType === 'วิชาการ') {
              await User.findOneAndUpdate(
                { _id: data.student },
                {
                  $inc: { hoursMeetingTechnic: -payload.meeting.hours }
                }
              )
            }
          }
        } else {
        }
        res.json(data)
      }
    )
  },
  async deleteMeetingJoin (req, res, next) {
    const meetingJoinId = req.params.id
    await MeetingJoin.findByIdAndDelete(meetingJoinId).then(async data => {
      await Meeting.findOneAndUpdate(
        { _id: data.meeting },
        {
          $inc: { getAmountStudent: -1 }
        }
      ).then(async dataMeeting => {
        if (data.statusCheckName === true) {
          if (dataMeeting.meetingType === 'เตรียมความพร้อม') {
            await User.findOneAndUpdate(
              { _id: data.student },
              {
                $inc: { hoursMeetingPrepare: -dataMeeting.hours }
              }
            )
          } else if (dataMeeting.meetingType === 'วิชาการ') {
            await User.findOneAndUpdate(
              { _id: data.student },
              {
                $inc: { hoursMeetingTechnic: -dataMeeting.hours }
              }
            )
          }
        }
      })
      res.json(data)
    })
  },
  async findstudentid (req, res, next) {
    const meetingJoinId = req.params.id
    console.log(meetingJoinId)
  }
}
