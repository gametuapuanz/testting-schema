const Position = require('./../model/positionsModel')

module.exports = {
  async getPositions (req, res, next) {
    await Position.find().then(data => {
      res.json(data)
    })
  },
  async addPosition (req, res, next) {
    const position = new Position(req.body)
    await position.save().then(data => {
      res.json(data)
    })
  },
  async updatePosition (req, res, next) {
    const payload = req.body
    await Position.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deletePosition (req, res, next) {
    const positionId = req.params.id
    await Position.findByIdAndDelete(positionId).then(data => {
      res.json(data)
    })
  }
}
