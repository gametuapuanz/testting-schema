const CommonModel = require('./../model/commonModel')

module.exports = {
  async getAllCommon (req, res, next) {
    await CommonModel.find().then(data => {
      res.json(data)
    })
  },
  async getAboutCooperative (req, res, next) {
    await CommonModel.find()
      .select('aboutCooperative')
      .then(data => {
        res.json(data)
      })
  },
  async getConditionCooperative (req, res, next) {
    await CommonModel.find()
      .select('conditionCooperative')
      .then(data => {
        res.json(data)
      })
  },
  async getStepCooperative (req, res, next) {
    await CommonModel.find()
      .select('stepCooperative')
      .then(data => {
        res.json(data)
      })
  },
  async updateAboutCooperative (req, res, next) {
    const aboutCooperative = req.body.aboutCooperative
    await CommonModel.updateOne(
      {},
      { aboutCooperative: aboutCooperative }
    ).then(data => {
      res.json(data)
    })
  },
  async updateConditionCooperative (req, res, next) {
    const conditionCooperative = req.body.conditionCooperative
    await CommonModel.updateOne(
      {},
      { conditionCooperative: conditionCooperative }
    ).then(data => {
      res.json(data)
    })
  },
  async updateStepCooperative (req, res, next) {
    const stepCooperative = req.body.stepCooperative
    await CommonModel.updateOne({}, { stepCooperative: stepCooperative }).then(
      data => {
        res.json(data)
      }
    )
  }
}
