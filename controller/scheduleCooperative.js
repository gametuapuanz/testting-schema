const ScheduleCooperative = require('./../model/scheduleCooperativeModel')

module.exports = {
  async getScheduleCooperatives (req, res, next) {
    await ScheduleCooperative.find(
      {},
      {},
      {
        sort: {
          date1: 1
        }
      }).then(data => {
      res.json(data)
    })
  },
  async getScheduleCooperative (req, res, next) {
    const scheduleCooperativeId = req.params.id
    await ScheduleCooperative.findById(scheduleCooperativeId).then(data => {
      res.json(data)
    })
  },
  async addScheduleCooperative (req, res, next) {
    const scheduleCooperative = new ScheduleCooperative(req.body)
    await scheduleCooperative.save().then(data => {
      res.json(data)
    })
  },
  async updateScheduleCooperative (req, res, next) {
    const payload = req.body
    await ScheduleCooperative.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteScheduleCooperative (req, res, next) {
    const scheduleCooperativeId = req.params.id
    await ScheduleCooperative.findByIdAndDelete(scheduleCooperativeId).then(data => {
      res.json(data)
    })
  }
}
