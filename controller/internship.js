const Internship = require('./../model/internshipModel')
const User = require('./../model/usersModel')
const CompanyEnroll = require('./../model/companyEnrollModel')

module.exports = {
  async getInternships (req, res, next) {
    await Internship.find(
      {},
      {},
      {
        sort: {
          _id: -1
        }
      }
    )
      .populate({ path: 'companyEnroll', populate: 'company' })
      .populate('student')
      .then(data => {
        res.json(data)
      })
  },
  async getInternship (req, res, next) {
    const internshipId = req.params.id
    await Internship.findById(internshipId).then(data => {
      res.json(data)
    })
  },
  async getInternshipByStudent (req, res, next) {
    const studentId = req.params.id
    await Internship.findOne({ student: studentId })
      .populate({ path: 'companyEnroll', populate: 'company' })
      .populate('student')
      .then(data => {
        res.json(data)
      })
  },
  async addInternship (req, res, next) {
    const internship = new Internship(req.body)
    await User.updateOne(
      { _id: internship.student },
      {
        $set: { statusCooperative: 1 }
      }
    )
    await internship.save().then(data => {
      res.json(data)
    })
  },
  async updateInternship (req, res, next) {
    const payload = req.body
    var statusCoo = 0
    await User.findOneAndUpdate(
      { _id: payload.student },
      {
        $set: { statusCooperative: payload.statusAttend }
      }
    ).then(data => {
      statusCoo = data.statusCooperative
    })
    if (statusCoo === 4) {
      await CompanyEnroll.findOneAndUpdate(
        { _id: payload.companyEnroll },
        {
          $inc: { getAmount: -1 }
        }
      )
    } else if (req.body.statusAttend === 4) {
      await CompanyEnroll.findOneAndUpdate(
        { _id: payload.companyEnroll },
        {
          $inc: { getAmount: 1 }
        }
      )
    }
    await Internship.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteInternship (req, res, next) {
    const internshipId = req.params.id
    await Internship.findByIdAndDelete(internshipId).then(async data => {
      if (data.statusAttend === 4) {
        await CompanyEnroll.findOneAndUpdate(
          { _id: data.companyEnroll },
          {
            $inc: { getAmount: -1 }
          }
        )
      }
      await User.findOneAndUpdate(
        { _id: data.student },
        {
          $set: { statusCooperative: 0 }
        }
      ).then(data => {
        res.json(data)
      })
    })
  }
}
