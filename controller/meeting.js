const Meeting = require('./../model/meetingModel')

module.exports = {
  async getMeetings (req, res, next) {
    await Meeting.find(
      {},
      {},
      {
        sort: {
          registDate1: -1
        }
      })
      .populate('company')
      .then(data => {
        res.json(data)
      })
  },
  async getMeeting (req, res, next) {
    const meetingId = req.params.id
    await Meeting.findById(meetingId).populate('company').then(data => {
      res.json(data)
    })
  },
  async addMeeting (req, res, next) {
    const meeting = new Meeting(req.body)
    console.log(meeting)
    await meeting.save().then(data => {
      res.json(data)
    })
  },
  async updateMeeting (req, res, next) {
    const payload = req.body
    await Meeting.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteMeeting (req, res, next) {
    const meetingId = req.params.id
    await Meeting.findByIdAndDelete(meetingId).then(data => {
      res.json(data)
    })
  }
}
