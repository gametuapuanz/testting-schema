const Skill = require('./../model/skillsModel')

module.exports = {
  async getSkills (req, res, next) {
    await Skill.find().then(data => {
      res.json(data)
    })
  },
  async addSkill (req, res, next) {
    const skill = new Skill(req.body)
    await skill.save().then(data => {
      res.json(data)
    })
  },
  async updateSkill (req, res, next) {
    const payload = req.body
    await Skill.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteSkill (req, res, next) {
    const skillId = req.params.id
    await Skill.findByIdAndDelete(skillId).then(data => {
      res.json(data)
    })
  }
}
