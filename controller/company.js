const Company = require('./../model/companyModel')

module.exports = {
  async getCompanys (req, res, next) {
    await Company.find().then(data => {
      res.json(data)
    })
  },
  async getCompany (req, res, next) {
    const companyId = req.params.id
    await Company.findById(companyId).then(data => {
      res.json(data)
    })
  },
  async addCompany (req, res, next) {
    const company = new Company(req.body)
    await company.save().then(data => {
      res.json(data)
    })
  },
  async updateCompany (req, res, next) {
    const payload = req.body
    await Company.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteCompany (req, res, next) {
    const companyId = req.params.id
    await Company.findByIdAndDelete(companyId).then(data => {
      res.json(data)
    })
  }
}
