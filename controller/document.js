const Document = require('./../model/documentModel')

module.exports = {
  async getDocuments (req, res, next) {
    await Document.find().then(data => {
      res.json(data)
    })
  },
  async getDocument (req, res, next) {
    const documentId = req.params.id
    await Document.findById(documentId).then(data => {
      res.json(data)
    })
  },
  async addDocument (req, res, next) {
    const payload = JSON.parse(req.body.json)
    payload.pathImage = req.file.filename
    payload.dateUpload = new Date()
    const document = new Document(payload)
    document.save().then(data => {
      res.json(data)
    })
  },
  async updateDocument (req, res, next) {
    const payload = req.body
    await Document.findByIdAndUpdate(payload._id, payload).then(data => {
      res.json(data)
    })
  },
  async deleteDocument (req, res, next) {
    const documentId = req.params.id
    await Document.findByIdAndDelete(documentId).then(data => {
      res.json(data)
    })
  }
}
